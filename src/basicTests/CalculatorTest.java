package basicTests;


//import java.util.Random;


public class CalculatorTest {
	interface Calculator{
		int calculate(int a, int b); 
	}
	
	public int doCalculation (int a, int b, Calculator calc){
		return calc.calculate(a, b);
		
	}
	
	public static void main(String[] args){
		CalculatorTest jools = new CalculatorTest();
		
		Calculator addition = (a,b) -> a+b;
		Calculator subtraction = (a,b) -> a-b;
		
		System.out.println(jools.doCalculation(4, 5, addition));
		System.out.println(jools.doCalculation(4, 5, subtraction));
		
	}
	
}
