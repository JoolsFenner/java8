package basicTests;

import lambdaBasics.*;
import basicTests.*;


public class Runner {
	public static void main(String[] args){

		/** 
		 * BASIC TESTS
		 */
		
//		new StringSortAnonInnerClass().doTests();
		
//		new StringSortNamedInnerClass().doTests();
		
//		new StringSortSeperateClass().doTests();
		
//		String[] strArray = new String[]{"cat", "dog", "mouse", "lizard"};
//		System.out.print(RandomUtils.randomElement(strArray));
		
		
		/** 
		 * LAMBDA BASICS 
		 */
		
		// basic lambdas
//		new ArraySort().doTests();
		
		// functional interface
		MathUtilities.doTests();
	
	
		
	
	}
}
