package basicTests;
import java.util.Arrays;

import utils.LastLetterComparator;
import utils.StringLengthComparator;

public class StringSortSeperateClass {
	public void doTests() {
		String[] testStrings = {"one", "two", "three", "four"};
		System.out.print("Original: ");
		printArray(testStrings);
		Arrays.sort(testStrings, new StringLengthComparator());
		System.out.print("After sorting by length: ");
		printArray(testStrings);
		Arrays.sort(testStrings, new LastLetterComparator());
		System.out.print("After sorting by last letter: ");
		printArray(testStrings);
	}
	
	public void printArray(String[] strArray){
		
		for(String next : strArray){
			System.out.println(next);
		}
	}
	
	
	
}