package java8Trail;

import java8Trail.RosterTest.CheckPerson;

//import java8Trail.RosterTest.CheckPerson;

public class CheckPersonEligibleForSelectiveService implements CheckPerson {

	@Override
	public boolean test(Person p) {
		return p.getGender() == Person.Sex.MALE && p.getAge() >= 18 && p.getAge() <= 45;
		
	}

}
