package java8Trail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java8Trail.CheckPersonEligibleForSelectiveService;
import java8Trail.Person;
import java8Trail.Person.Sex;
import java8Trail.RosterTest;
import java8Trail.RosterTest.CheckPerson;
public class Java8RosterRunner{

	public static void main(String[] args){
		/** 
		 * JAVA 8 TRAIL
		 * http://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html 
		 */
	
		List<Person> roster = new ArrayList<Person>();
		roster.add(new Person("Julian Fenner", Sex.MALE, LocalDate.parse("1981-02-06"),"julian@gmail.com"));
		roster.add(new Person("Linda Florence", Sex.FEMALE, LocalDate.parse("1981-10-23"),"linda@gmail.com"));
		roster.add(new Person("Andrea Fenner", Sex.FEMALE, LocalDate.parse("1977-10-26"),"andrea@gmail.com"));
		roster.add(new Person("Graciela Fenner", Sex.FEMALE, LocalDate.parse("1946-03-13"),"graciela@gmail.com"));
		roster.add(new Person("David Fenner", Sex.MALE, LocalDate.parse("1946-10-18"),"david@gmail.com"));
		
		//Approach 1: Create Methods That Search for Members That Match One Characteristic
//		RosterTest.printPersonsOlderThan(roster, 34);
//		System.out.println();
		
		//Approach 2: Create More Generalized Search Methods
//		RosterTest.printPersonsWithinAgeRange(roster, 32, 40);
//		System.out.println();
	
		//Approach 3: Specify Search Criteria Code in a Local Class
//		RosterTest.printPersons(roster, new CheckPersonEligibleForSelectiveService());
//		System.out.println();
		
//		//Approach 4: Specify Search Criteria Code in an Anonymous Class
//		RosterTest.printPersons(
//			    roster,
//			    new CheckPerson() {
//			        public boolean test(Person p) {
//			            return p.getGender() == Person.Sex.FEMALE
//			                && p.getAge() >= 35
//			                && p.getAge() <= 60;
//			        }
//			    }
//			);

//		//Approach 5: Specify Search Criteria Code with a Lambda Expression
//		RosterTest.printPersons(
//			    roster,
//			    (Person p) -> p.getGender() == Person.Sex.MALE
//			        && p.getAge() >= 40
//			        && p.getAge() <= 70
//			);
		
		//Approach 6: Use Standard Functional Interfaces with Lambda Expressions
		
//		RosterTest.printPersonsWithPredicate(
//			    roster,
//			    p -> p.getGender() == Person.Sex.MALE
//			        && p.getAge() >= 18
//			        && p.getAge() <= 75
//			);
		 
//		//Approach 7: Use Lambda Expressions Throughout Your Application
//		RosterTest.processPersons(
//				roster, 
//				(Person p) -> p.getAge() < 50, 
//				p -> p.printPerson()
//		);
		
		
		
		
		// Approach 7, second example
//		RosterTest.processPersonsWithFunction(
//				roster,
//				p -> p.getGender()== Person.Sex.FEMALE
//					&& p.getAge() < 40,
//				p -> p.getEmailAddress(),
//				email ->System.out.println(email)
//		);
		
//		 // Approach 8: Use Generics More Extensively
//		RosterTest.processElements(
//				roster,
//				p -> p.getGender()== Person.Sex.FEMALE
//					&& p.getAge() < 40,
//				p -> p.getEmailAddress(),
//				email ->System.out.println(email)
//		);
		
		//Approach 9: Use Bulk Data Operations That Accept Lambda Expressions as Parameters

//	  System.out.println("Persons who are eligible for Selective Service " +
//	  "(with bulk data operations):");
//	
//	  roster
//	      .stream()
//	      .filter(
//	          p -> p.getGender() == Person.Sex.FEMALE
//	              && p.getAge() >= 30
//	              && p.getAge() <= 40)
//	      .map(p -> p.getEmailAddress())
//	      .forEach(email -> System.out.println(email));
		
		//Using Methods References
		Person[] rosterAsArray = roster.toArray(new Person[roster.size()]);
		for(Person next : rosterAsArray) next.printPerson();
		
		Arrays.sort(rosterAsArray, Person::compareByAge);
		/* the statement above is semantically the same as...
		 Arrays.sort(rosterAsArray,
			    (a, b) -> Person.compareByAge(a, b)
			);
		*/
		System.out.println();
		System.out.println("after sort");
		for(Person next : rosterAsArray) next.printPerson();
		
	}
	
	
	
	
}