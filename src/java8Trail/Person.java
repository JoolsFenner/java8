package java8Trail;

import java.time.LocalDate;
import java.time.Period;
import java8Trail.Person.Sex;

public class Person {
	
	public enum Sex {
        MALE, FEMALE
    }
	
	private String name;
    private LocalDate birthdate;
    private Sex gender;
    private String email;
   

    LocalDate today = LocalDate.now();
	public Person(String name, Sex gender, LocalDate birthdate, String email){
		this.gender = gender;
		this.name = name;
		this.birthdate = birthdate;
		this.email = email;
		
	}
		
	
	public int getAge(){
		LocalDate now = LocalDate.now();
		Period p = Period.between(birthdate, now);
		return p.getYears();
	}
    
	public void printPerson(){
		System.out.println(name);
		System.out.println(getAge());
		
	}


	public Sex getGender() {
		return gender;
	}
	
	public String getEmailAddress(){
		return email;
	}
	
	public static int compareByAge(Person a, Person b) {
        return a.birthdate.compareTo(b.birthdate);
    }
  
}