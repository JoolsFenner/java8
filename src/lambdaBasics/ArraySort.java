package lambdaBasics;

import java.util.Arrays;

import utils.ArrayUtils;

public class ArraySort {
	public void doTests(){
		String[] testString = new String[]{"mouse", "cat", "bat", "zebra", "lion"};
		
		System.out.println("pre sort");
		ArrayUtils.printArray(testString);
		System.out.println();
		
		//with type inferencing and return value
		System.out.println("sort first letter");
		Arrays.sort(testString,(String s1, String s2) -> { return(s1.length() - s2.length()); });
		ArrayUtils.printArray(testString);
		System.out.println();
		
		//without type inferencing and implied return value;
		System.out.println("sort last letter");
		Arrays.sort(testString,(s1, s2) ->  s1.charAt(s1.length()-1) - s2.charAt(s2.length()-1));
		ArrayUtils.printArray(testString);
		System.out.println();
		
	}
}
