package lambdaBasics;

@FunctionalInterface
public interface Integrable {
	double eval(double x);
}
