package lambdaBasics;


public class MathUtilities {

	public static double integrate(Integrable function, double x1, double x2, int numSlices){
		
		if(numSlices < 1){
			numSlices = 1;
		}
		
		double delta = (x2 - x1) / numSlices;
		double start = x1 + delta /2;
		double sum = 0;
		
		for(int i=0; i < numSlices; i++){
			sum += delta * function.eval(start + delta * i);
		}
		
		return(sum);
	}
	
	public static void integrationTest(Integrable function,double x1, double x2) {
		for(int i=1; i<7; i++) {
			int numSlices = (int)Math.pow(10, i);
			double result = MathUtilities.integrate(function, x1, x2, numSlices);
			System.out.printf(" For numSlices =%,10d result = %,.8f%n",numSlices, result);
		}
	}
	
	public static void doTests(){
		
		//Code that calls methods that expect 1-­‐method interfaces can now use lambdas
		System.out.println();
		System.out.println("integrationTest(x -> x*x, 10, 100)");
		integrationTest(x -> x*x, 10, 100);
		
		System.out.println();
		System.out.println("integrationTest(x -> Math.pow(x,3), 50, 500);");
		integrationTest(x -> Math.pow(x,3), 50, 500);
		
		System.out.println();
		System.out.println("integrationTest(x -> Math.sin(x), 0, Math.PI);");
		integrationTest(x -> Math.sin(x), 0, Math.PI);
		
		System.out.println();
		System.out.println("integrationTest(x -> Math.exp(x), 2, 20);");
		integrationTest(x -> Math.exp(x), 2, 20);
		
	}
}
